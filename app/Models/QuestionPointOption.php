<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionPointOption extends Model
{
    use HasFactory;

    protected $table = 'questionPointOptions';

    protected $fillable = ['questionPoint_id', 'option', 'order'];

    public function point(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(QuestionPoint::class, 'questionPoint_id');
    }
}
