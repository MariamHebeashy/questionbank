<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionPoint extends Model
{
    use HasFactory;

    protected $table = 'questionPoints';

    protected $fillable = ['question_id', 'point', 'order', 'answer'];

    public function question(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Question::class);
    }

    public function options(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(QuestionPointOption::class, 'questionPoint_id');
    }
}
