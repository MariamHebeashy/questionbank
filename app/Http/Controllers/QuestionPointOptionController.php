<?php

namespace App\Http\Controllers;

use App\Models\QuestionPointOption;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class QuestionPointOptionController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param QuestionPointOption $questionPointOption
     * @return bool
     */
    public function update(Request $request, QuestionPointOption $questionPointOption): bool
    {
        $request->validate([
            'option' => 'required',
            'order' => 'required', Rule::in(['A', 'B', 'C', 'D']),
        ]);

        $questionPointOption->update([
            'option' => $request->option,
            'order' => $request->order,
        ]);
        return true;
    }
}
