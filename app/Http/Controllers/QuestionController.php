<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\QuestionPoint;
use App\Models\QuestionPointOption;
use App\Models\Subject;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use PhpOffice\PhpWord\Exception\Exception;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use File;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|\Illuminate\Foundation\Application|\Illuminate\View\View|View
     */
    public function index()
    {
        $questions = Question::with(['subject','points'])->get();
        return view('question.index',compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|\Illuminate\Foundation\Application|\Illuminate\View\View|View
     */
    public function create()
    {
        $subjects = Subject::all();
        return view('question.create',compact('subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {

        $request->validate([
            'subject' => 'required|exists:App\Models\Subject,id',
            'question' => 'required',
            'level' => 'required', Rule::in(['hard', 'medium', 'easy']),
            'points' => 'required|array',
        ]);
        $question = Question::create([
            'subject_id' => $request->subject,
            'question' => $request->question,
            'level' => $request->level,
        ]);

        $optionsOrder = [0 => 'A', 1 => 'B', 2 => 'C', 3 => 'D'];
        for ($x = 1; $x <= 3; $x++) {
            $questionPoint = QuestionPoint::create([
                'question_id' => $question->id,
                'point' => $request->points[$x]['point'],
                'order' => $x,
                'answer' => $request->points[$x]['Answer'],
            ]);
            for ($y = 0; $y <= 3; $y++) {
                QuestionPointOption::create([
                    'questionPoint_id' => $questionPoint->id,
                    'option' => $request->points[$x]['options'][$optionsOrder[$y]],
                    'order' => $optionsOrder[$y],
                ]);
            }
        }
        return redirect()->route('questions.index')->with('success','Question created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param Question $question
     * @return Application|Factory|\Illuminate\Foundation\Application|\Illuminate\View\View|View
     */
    public function show(Question $question)
    {
        return view('question.show',compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Question $question
     * @return Application|Factory|\Illuminate\Foundation\Application|\Illuminate\View\View|View
     */
    public function edit($question_id)
    {
        $question = Question::where('id', $question_id)->with(['subject', 'points'])->first();
        $subjects = Subject::all();
        return view('question.edit',compact('question', 'subjects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Question $question
     * @return RedirectResponse
     */
    public function update(Request $request, Question $question): RedirectResponse
    {
        $request->validate([
            'subject' => 'required|exists:App\Models\Subject,id',
            'question' => 'required',
            'level' => 'required', Rule::in(['hard', 'medium', 'easy']),
        ]);
//        return $request->all();
        $question->update([
            'subject_id' => $request->subject,
            'question' => $request->question,
            'level' => $request->level,
        ]);

        $optionsOrder = [0 => 'A', 1 => 'B', 2 => 'C', 3 => 'D'];
        for ($x = 1; $x <= 3; $x++) {
            $questionPoint = QuestionPoint::where('id')->update([
                'question_id' => $question->id,
                'point' => $request->points[$x]['point'],
                'order' => $x,
                'answer' => $request->points[$x]['Answer'],
            ]);
        }
        $optionsOrder = [0 => 'A', 1 => 'B', 2 => 'C', 3 => 'D'];
        for ($x = 1; $x <= 3; $x++) {
            $questionPoint = QuestionPoint::where('id', $request->points[$x]['id'])->update([
                'point' => $request->points[$x]['point'],
                'answer' => $request->points[$x]['Answer'],
            ]);
            for ($y = 0; $y <= 3; $y++) {
                QuestionPointOption::where('id', $request->points[$x]['options'][$optionsOrder[$y]]['id'])->update([
                    'option' => $request->points[$x]['options'][$optionsOrder[$y]]['option'],
                ]);
            }
        }
        return redirect()->route('questions.index')
            ->with('success','Question updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Question $question
     * @return RedirectResponse
     */
    public function destroy(Question $question): RedirectResponse
    {
        $question->delete();

        return redirect()->route('questions.index')
            ->with('success','Question deleted successfully');
    }

    /**
     * @throws Exception
     */
    public function createExam(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        $examQuestions[] = Question::where('subject_id', 1)->where('level', 'easy')->inRandomOrder()->limit(1)->with('points')->first();
        $examQuestions[] = Question::where('subject_id', 1)->where('level', 'medium')->inRandomOrder()->limit(1)->with('points')->first();
        $examQuestions[] = Question::where('subject_id', 1)->where('level', 'hard')->inRandomOrder()->limit(1)->with('points')->first();

        $phpWord = new PhpWord();
        $section = $phpWord->addSection();

        $order = [0 => 'First', 1 => 'Second', 2 => 'Third'];
        foreach ($examQuestions as $key => $question){
            $section->addText(
                $order[$key] . ' ' . $question->question
            );
            foreach ($question['points'] as $point){
                $section->addText(
                     $point->order . '- ' . $point->point . '
                     '
                );
                foreach ($point['options'] as $option){
                    $section->addText(
                        $option->order . '- ' . $option->option . '
                        '
                    );
                }
                $section->addText('');
            }
        }
        $exams = count(File::files(public_path() . '/exams'));
        $objWriter = IOFactory::createWriter($phpWord, 'ODText');
        $objWriter->save('exams/exam' . ($exams + 1) . '.odt');
        return response()->download(public_path('exams/exam' . ($exams + 1) . '.odt'));
    }
}
