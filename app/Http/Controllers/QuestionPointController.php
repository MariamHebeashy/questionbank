<?php

namespace App\Http\Controllers;

use App\Models\QuestionPoint;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class QuestionPointController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param QuestionPoint $questionPoint
     * @return bool
     */
    public function update(Request $request, QuestionPoint $questionPoint): bool
    {
        $request->validate([
            'point' => 'required',
            'order' => 'required', Rule::in(['A', 'B', 'C']),
            'answer' => 'required', Rule::in(['A', 'B', 'C', 'D']),
        ]);

        $questionPoint->update([
            'point' => $request->point,
            'order' => $request->order,
            'answer' => $request->answer,
        ]);

        return true;
    }
}
