-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: questionBank
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `questionPointOptions`
--

DROP TABLE IF EXISTS `questionPointOptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `questionPointOptions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `questionPoint_id` int NOT NULL,
  `option` text NOT NULL,
  `order` enum('A','B','C','D') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_questionPointOptions_1_idx` (`questionPoint_id`),
  CONSTRAINT `fk_questionPointOptions_1` FOREIGN KEY (`questionPoint_id`) REFERENCES `questionPoints` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionPointOptions`
--

LOCK TABLES `questionPointOptions` WRITE;
/*!40000 ALTER TABLE `questionPointOptions` DISABLE KEYS */;
INSERT INTO `questionPointOptions` VALUES (1,2,'001111','A','2021-08-29 08:27:00','2021-08-29 08:27:00'),(2,2,'101111','B','2021-08-29 08:27:00','2021-08-29 08:27:00'),(3,2,'101110','C','2021-08-29 08:27:00','2021-08-29 08:27:00'),(4,2,'001110','D','2021-08-29 08:27:00','2021-08-29 08:27:00'),(5,3,'at most (n – 1)/2 keys','A','2021-08-29 08:27:00','2021-08-29 08:27:00'),(6,3,'exact (n – 1)/2 keys','B','2021-08-29 08:27:00','2021-08-29 08:27:00'),(7,3,'at least 2n keys','C','2021-08-29 08:27:00','2021-08-29 08:27:00'),(8,3,'at least (n – 1)/2 keys','D','2021-08-29 08:27:00','2021-08-29 08:27:00'),(9,4,'AVL tree','A','2021-08-29 08:27:00','2021-08-29 08:27:00'),(10,4,'B-tree','B','2021-08-29 08:27:00','2021-08-29 08:27:00'),(11,4,'Red-black tree','C','2021-08-29 08:27:00','2021-08-29 08:27:00'),(12,4,'Both AVL tree and Red-black tree','D','2021-08-29 08:27:00','2021-08-29 08:27:00'),(13,5,'Larger instances of different problems','A','2021-08-29 08:34:08','2021-08-29 08:34:08'),(14,5,'Larger instances of the same problem','B','2021-08-29 08:34:08','2021-08-29 08:34:08'),(15,5,'Smaller instances of the same problem','C','2021-08-29 08:34:08','2021-08-29 08:34:08'),(16,5,'Smaller instances of different problems','D','2021-08-29 08:34:08','2021-08-29 08:34:08'),(17,6,'Factorial of a number','A','2021-08-29 08:34:08','2021-08-29 08:34:08'),(18,6,'Nth fibonacci number','B','2021-08-29 08:34:08','2021-08-29 08:34:08'),(19,6,'Length of a string','C','2021-08-29 08:34:08','2021-08-29 08:34:08'),(20,6,'Problems without base case','D','2021-08-29 08:34:08','2021-08-29 08:34:08'),(21,7,'Switch Case','A','2021-08-29 08:34:08','2021-08-29 08:34:08'),(22,7,'Loop','B','2021-08-29 08:34:08','2021-08-29 08:34:08'),(23,7,'If-else','C','2021-08-29 08:34:08','2021-08-29 08:34:08'),(24,7,'if elif else','D','2021-08-29 08:34:08','2021-08-29 08:34:08'),(25,8,'Playing a game on Computer','A','2021-08-29 08:41:54','2021-08-29 08:41:54'),(26,8,'Making a machine Intelligent','B','2021-08-29 08:41:54','2021-08-29 08:41:54'),(27,8,'Programming on Machine with your Own Intelligence','C','2021-08-29 08:41:54','2021-08-29 08:41:54'),(28,8,'Putting your intelligence in Machine','D','2021-08-29 08:41:54','2021-08-29 08:41:54'),(29,9,'Real-life situation','A','2021-08-29 08:41:54','2021-08-29 08:41:54'),(30,9,'Small Search Space','B','2021-08-29 08:41:54','2021-08-29 08:41:54'),(31,9,'Complex game','C','2021-08-29 08:41:54','2021-08-29 08:41:54'),(32,9,'All of the above','D','2021-08-29 08:41:54','2021-08-29 08:41:54'),(33,10,'Optimal Search','A','2021-08-29 08:41:54','2021-08-29 08:41:54'),(34,10,'Depth First Search','B','2021-08-29 08:41:54','2021-08-29 08:41:54'),(35,10,'Breadth-First Search','C','2021-08-29 08:41:54','2021-08-29 08:41:54'),(36,10,'Linear Search','D','2021-08-29 08:41:54','2021-08-29 08:41:54'),(37,14,'Java','A','2021-08-29 10:01:20','2021-08-29 11:16:41'),(38,14,'C++','B','2021-08-29 10:01:20','2021-08-29 11:16:41'),(39,14,'Kotlin','C','2021-08-29 10:01:21','2021-08-29 11:16:41'),(40,14,'SmallTalk','D','2021-08-29 10:01:21','2021-08-29 11:16:41'),(41,15,'Parent of an object','A','2021-08-29 10:01:21','2021-08-29 11:16:42'),(42,15,'Instance of an object','B','2021-08-29 10:01:21','2021-08-29 11:16:42'),(43,15,'Blueprint of an object','C','2021-08-29 10:01:21','2021-08-29 11:16:42'),(44,15,'Scope of an object','D','2021-08-29 10:01:21','2021-08-29 11:16:43'),(45,16,'Code reusability','A','2021-08-29 10:01:22','2021-08-29 11:16:43'),(46,16,'Modularity','B','2021-08-29 10:01:22','2021-08-29 11:16:43'),(47,16,'Duplicate/Redundant data','C','2021-08-29 10:01:22','2021-08-29 11:16:43'),(48,16,'Efficient Code','D','2021-08-29 10:01:22','2021-08-29 11:16:44');
/*!40000 ALTER TABLE `questionPointOptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionPoints`
--

DROP TABLE IF EXISTS `questionPoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `questionPoints` (
  `id` int NOT NULL AUTO_INCREMENT,
  `question_id` int NOT NULL,
  `point` text NOT NULL,
  `order` enum('1','2','3') NOT NULL,
  `answer` enum('A','B','C','D') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_questionPoints_1_idx` (`question_id`),
  CONSTRAINT `fk_questionPoints_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionPoints`
--

LOCK TABLES `questionPoints` WRITE;
/*!40000 ALTER TABLE `questionPoints` DISABLE KEYS */;
INSERT INTO `questionPoints` VALUES (2,1,' Express -15 as a 6-bit signed binary number','1','B','2021-08-29 08:23:51','2021-08-29 11:52:07'),(3,1,'B-tree of order n is a order-n multiway tree in which each non-root node contains','2','D','2021-08-29 08:23:51','2021-08-29 11:52:07'),(4,1,'Which of the following is the most widely used external memory data structure','3','B','2021-08-29 08:23:51','2021-08-29 11:52:07'),(5,2,'Recursion is a method in which the solution of a problem depends on','1','C','2021-08-29 08:30:44','2021-08-29 11:52:07'),(6,2,'Which of the following problems can’t be solved using recursion?','2','D','2021-08-29 08:30:44','2021-08-29 11:52:07'),(7,2,'Recursion is similar to which of the following','3','B','2021-08-29 08:30:44','2021-08-29 11:52:07'),(8,3,'Artificial Intelligence is about','1','B','2021-08-29 08:38:01','2021-08-29 11:52:07'),(9,3,'Select the most appropriate situation for that a blind search can be used','2','B','2021-08-29 08:38:01','2021-08-29 11:52:07'),(10,3,'Among the given options, which search algorithm requires less memory?','3','B','2021-08-29 08:38:01','2021-08-29 11:52:07'),(14,9,'Which was the first purely object oriented programming language developed','1','D','2021-08-29 10:01:20','2021-08-29 11:16:41'),(15,9,'Which of the following best defines a class','2','C','2021-08-29 10:01:21','2021-08-29 11:16:42'),(16,9,'Which is not feature of OOP in general definitions','3','C','2021-08-29 10:01:22','2021-08-29 11:16:43');
/*!40000 ALTER TABLE `questionPoints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `questions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `subject_id` int NOT NULL,
  `question` varchar(255) NOT NULL,
  `level` enum('hard','medium','easy') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_questions_1_idx` (`subject_id`),
  CONSTRAINT `fk_questions_1` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,1,'Data Structure','easy','2021-08-29 08:13:26','2021-08-29 08:27:29',NULL),(2,1,'algorithm','medium','2021-08-29 08:14:55','2021-08-29 08:14:55',NULL),(3,1,'AI','hard','2021-08-29 08:16:07','2021-08-29 08:16:07',NULL),(9,1,'oop','medium','2021-08-29 10:01:20','2021-08-29 13:31:27',NULL);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subjects` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects`
--

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
INSERT INTO `subjects` VALUES (1,'CS','2021-08-29 08:12:47','2021-08-29 08:12:47');
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-29 14:13:23
