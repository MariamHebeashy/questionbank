
# Question Bank

This is a simple Question Bank Application.




## Deployment

To deploy this project run

```bash
  git clone https://MariamHebeashy@bitbucket.org/MariamHebeashy/questionbank.git

  cd questionBank

  composer install
```




## Database Configurations

```bash
  CREATE SCHEMA `questionBank` DEFAULT CHARACTER SET utf8 ;
```

Then import `Dump20210829.sql` file To created Schema

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`DB_HOST`

`DB_DATABASE`

`DB_USERNAME`

`DB_PASSWORD`


## URls

- `{base_url}/questions`

  
