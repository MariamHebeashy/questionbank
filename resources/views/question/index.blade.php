@extends('layout.master')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">questions</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Questions List</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">All questions</h6>
            </nav>
            <div class="col-6 text-end">
                <a class="btn bg-gradient-dark mb-0" href="{{ route('generateExam') }}"><i class="fas fa-file-word" aria-hidden="true"></i>&nbsp;&nbsp;Generate Exam</a>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h6 class="mb-0">Questions</h6>
                            </div>
                            <div class="col-6 text-end">
                                <a class="btn btn-outline-primary btn-sm mb-0" href="{{ route('questions.create') }}">
                                    <i class="fas fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add New Question</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center justify-content-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Question</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">level</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Subject</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Created At</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($questions as $question)
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2">
                                                <div class="my-auto">
                                                    <h6 class="mb-0 text-sm">{{ $question->question }}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-sm font-weight-bold mb-0">{{ $question->level }}</p>
                                        </td>
                                        <td>
                                            <span class="text-xs font-weight-bold">{{ $question->subject['name'] }}</span>
                                        </td>
                                        <td>
                                            <span class="text-xs font-weight-bold">{{ $question->created_at }}</span>
                                        </td>
                                        <td class="align-middle">
{{--                                            <button class="btn btn-link text-secondary mb-0">--}}
{{--                                                <i class="fa fa-ellipsis-v text-xs"></i>--}}
{{--                                            </button>--}}
                                            <form action="{{ route('questions.destroy', $question->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-link text-danger text-gradient px-3 mb-0" type="submit"><i class="far fa-trash-alt me-2" aria-hidden="true"></i>Delete</button>
                                            </form>
                                            <a class="btn btn-link text-dark px-3 mb-0" href="{{ route('questions.edit', $question->id) }}">
                                                <i class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
