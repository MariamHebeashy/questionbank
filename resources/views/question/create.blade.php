@extends('layout.master')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">questions</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Add New</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">New question</h6>
            </nav>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-3">
                        <div class="row">
                            <form action='{{ route('questions.store') }}' method='Post'>
                                @csrf
                                <ul class="list-group">
                                    <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                        <div class="mb-3 col-12">
                                            <label class="form-label" for="subject">Question`s Subject</label>
                                            <select class="form-select" name="subject" id="subject" aria-label="Default select example" required>
                                                <option selected>Select Subject</option>
                                                @foreach($subjects as $subject)
                                                    <option value="{{ $subject['id'] }}">{{ $subject['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </li>
                                    <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                        <div class="mb-3 col-12">
                                            <div class="content">
                                                <label class="form-label" for="question">Question Title</label>
                                                <input class="form-control" name="question" id="question" type="text" placeholder="Question Title" required>
                                            </div><br>
                                            <div class="content">
                                                <label class="form-label" for="QuestionLevel">Question level</label>
                                                <select class="form-select" name="level" id="QuestionLevel" aria-label="Default select example" required>
                                                    <option value="easy" selected>Easy</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="hard">hard</option>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    @for ($i = 1; $i <= 3; $i++)
                                        <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                            <div class="mb-3 col-12">
                                                <div class="content">
                                                    <h4>Point {{$i}}</h4>
                                                </div><br>
                                                <div class="content">
                                                    <label class="form-label" for="point{{$i}}Question">Point {{ $i }} Question</label>
                                                    <textarea class="form-control" name="points[{{$i}}][point]" id="point{{$i}}Question" rows="3" required></textarea>
                                                </div><br>
                                                <div class="content">
                                                    <div class="row">
                                                        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                                                            <label class="form-label" for="point{{$i}}OptionA">option A</label>
                                                            <textarea class="form-control" name="points[{{$i}}][options][A]" id="point{{$i}}OptionA" rows="3" required></textarea>
                                                        </div>
                                                        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                                                            <label class="form-label" for="point{{$i}}OptionB">option B</label>
                                                            <textarea class="form-control" name="points[{{$i}}][options][B]" id="point{{$i}}OptionB" rows="3" required></textarea>
                                                        </div>
                                                        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                                                            <label class="form-label" for="point{{$i}}OptionC">option C</label>
                                                            <textarea class="form-control" name="points[{{$i}}][options][C]" id="point{{$i}}OptionC" rows="3" required></textarea>
                                                        </div>
                                                        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                                                            <label class="form-label" for="point{{$i}}OptionD">option D</label>
                                                            <textarea class="form-control" name="points[{{$i}}][options][D]" id="point{{$i}}OptionD" rows="3" required></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content">
                                                    <label class="form-label" for="Answer">Answer</label>
                                                    <select class="form-select" name="points[{{$i}}][Answer]" id="Answer" aria-label="Default select example" required>
                                                        <option value="A" selected>A</option>
                                                        <option value="B">B</option>
                                                        <option value="C">C</option>
                                                        <option value="D">D</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                    @endfor
                                    <li>
                                        <div class="card-header pb-0 p-3">
                                            <div class="row">
                                                <div class="col-6 text-end">
                                                    <button type="submit" class="btn bg-gradient-dark mb-0" href="javascript:;">
                                                        <i class="fas fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Save Question
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
