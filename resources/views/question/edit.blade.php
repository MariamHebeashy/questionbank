@extends('layout.master')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">questions</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Edit</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Edit Question</h6>
            </nav>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-3">
                        <div class="row">
                            <form action='{{ route('questions.update', $question->id) }}' method='POST'>
                                @csrf
                                @method('PUT')
                                <ul class="list-group">
                                    <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                        <div class="mb-3 col-12">
                                            <label class="form-label" for="subject">Question`s Subject</label>
                                            <select class="form-select" name="subject" id="subject" aria-label="Default select example" required>
                                                @foreach($subjects as $subject)
                                                    <option value="{{ $subject['id'] }}" {{ ($question['subject_id'] == $subject['id']) ? 'selected' : '' }}>
                                                        {{ $subject['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </li>
                                    <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                        <div class="mb-3 col-12">
                                            <div class="content">
                                                <label class="form-label" for="question">Question Title</label>
                                                <input class="form-control" name="question" id="question" type="text" value="{{ $question['question'] }}"
                                                       placeholder="Question Title" required>
                                            </div><br>
                                            <div class="content">
                                                <label class="form-label" for="QuestionLevel">Question level</label>
                                                <select class="form-select" name="level" id="QuestionLevel" aria-label="Default select example" required>
                                                    <option value="easy" {{ ($question['level'] == 'easy') ? 'selected' : '' }}>Easy</option>
                                                    <option value="medium" {{ ($question['level'] == 'medium') ? 'selected' : '' }}>Medium</option>
                                                    <option value="hard" {{ ($question['level'] == 'hard') ? 'selected' : '' }}>hard</option>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    @foreach ($question['points'] as $key => $point)
                                        <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                            <div class="mb-3 col-12">
                                                <div class="content">
                                                    <h4>Point {{ $point['order'] }}</h4>
                                                </div><br>
                                                <div class="content">
                                                    <label class="form-label" for="point{{ $point['order'] }}Question">Point {{ $point['order'] }} Question</label>
                                                    <textarea class="form-control" name="points[{{ $point['order'] }}][point]" id="point{{ $point['order'] }}Question" rows="3" required>{{ $point['point'] }}</textarea>
                                                </div><br>
                                                <div class="content">
                                                    <div class="row">
                                                        @foreach($point['options'] as $option)
                                                            <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                                                                <label class="form-label" for="point{{ $point['order'] }}Option{{ $option['order'] }}">option {{ $option['order'] }}</label>
                                                                <textarea class="form-control" name="points[{{ $point['order'] }}][options][{{ $option['order'] }}][option]"
                                                                          id="point{{ $point['order'] }}Option{{ $option['order'] }}" rows="3" required>{{ $option['option'] }}</textarea>
                                                            </div>
                                                            <input type="hidden" name="points[{{ $point['order'] }}][options][{{ $option['order'] }}][id]" value="{{ $option['id'] }}">
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <div class="content">
                                                    <label class="form-label" for="Answer">Answer</label>
                                                    <select class="form-select" name="points[{{ $point['order'] }}][Answer]" id="Answer" aria-label="Default select example" required>
                                                        <option value="A" {{ ($point['answer'] == 'A') ? 'selected' : '' }}>A</option>
                                                        <option value="B" {{ ($point['answer'] == 'B') ? 'selected' : '' }}>B</option>
                                                        <option value="C" {{ ($point['answer'] == 'C') ? 'selected' : '' }}>C</option>
                                                        <option value="D" {{ ($point['answer'] == 'D') ? 'selected' : '' }}>D</option>
                                                    </select>
                                                </div>
                                                <input type="hidden" name="points[{{ $point['order'] }}][id]" value="{{ $point['id'] }}">
                                            </div>
                                        </li>
                                    @endforeach
                                    <li>
                                        <div class="card-header pb-0 p-3">
                                            <div class="row">
                                                <div class="col-6 text-end">
                                                    <button type="submit" class="btn bg-gradient-dark mb-0" href="javascript:;">
                                                        <i class="fas fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Save Question
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
